module.exports = function(robot) {

    robot.respond(/check servers/, function(res) {
        res.send("Checking servers ...");
    });

    robot.on('healthcheck:url', function(url) {
        robot.logger.info("Doing healthcheck for: " + url);
        robot.http(url).get()(function(err, res, body) {
            robot.logger.info(url + ' is ' + res.statusCode);
            if(err) {
                robot.logger.error(url + "respond " + res.statusCode);
                robot.logger.error(err);
                robot.send({room: process.env.HUBOT_NOTIFICATIONS_ROOM}, err);
            } else if(!/^2[0-9].*$/.test(res.statusCode)) {
                robot.logger.error(url + "respond " + res.statusCode);
                robot.logger.error(res.headers);
                robot.send({room: process.env.HUBOT_NOTIFICATIONS_ROOM}, "Error, " + url + " respond " + res.statusCode);
            }
        });
    });

    var healthcheckConfig = {
        "http://pe.arxcf.com/services/languages": {
            "watchInterval": 15000
        },
        "http://arxcorporatefinance.com/fr/accueil/": {
            "watchInterval": 15000
        },
        "http://master.arxcf.com/services/languages": {
            "watchInterval": 120000
        },
        "http://develop.arxcf.com/services/languages": {
            "watchInterval": 120000
        },
        "http://feature.arxcf.com/services/languages": {
            "watchInterval": 120000
        },
        "http://cry.databridge.fr/services/accounts/session": {
            "watchInterval": 60000
        }
    };

    for(var url in healthcheckConfig) {
        robot.logger.info("Registering healthcheck for: '" + url + "' every " + healthcheckConfig[url].watchInterval + "ms");
        (function(nestedUrl) {
            setInterval(function(){
                robot.emit('healthcheck:url', nestedUrl);
            }, healthcheckConfig[url].watchInterval);
        })(url);
    }
};